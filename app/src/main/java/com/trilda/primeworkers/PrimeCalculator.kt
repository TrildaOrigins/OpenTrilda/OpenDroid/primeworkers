package com.trilda.primeworkers

fun calculatePrimeNumbers(lowBound: Long, highBound: Long): MutableList<Long> {
    val primeNumbers = mutableListOf<Long>()
    var low = lowBound

    while (low < highBound) {
        var nonPrime = false

        for (i in 2..low / 2) {
            if (low % i == 0L) {
                nonPrime = true
                break
            }
        }

        if (!nonPrime) {
            primeNumbers.add(low)
        }

        ++low
    }
    return primeNumbers
}