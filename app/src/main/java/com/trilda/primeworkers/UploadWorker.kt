package com.trilda.primeworkers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters

class UploadWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    override fun doWork(): Result {

        Log.i("UploadWorker:", "Starting uploading...")
        Thread.sleep(1000)

        val listSize = MainActivity.primeNumbersList.size
        Log.i("UploadWorker:", "ListSize: $listSize")
        Thread.sleep(1000)
        val list = MainActivity.primeNumbersList

        Log.i("UploadWorker:", "List: $list")
        Thread.sleep(2000)

        Log.i("UploadWorker:", "uploading finished!")

        return Result.success()
    }
}